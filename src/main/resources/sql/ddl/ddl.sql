-- まずは削除
DROP TYPE IF EXISTS m_task;

-- タスク 管理テーブル
CREATE TABLE "m_task" (
  "task_id" integer PRIMARY KEY, -- タスク ID(PK)
  "task" varchar NOT NULL, -- タスク内容
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP) -- 登録日時
);