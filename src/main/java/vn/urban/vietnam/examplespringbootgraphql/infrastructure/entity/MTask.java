package vn.urban.vietnam.examplespringbootgraphql.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.urban.vietnam.examplespringbootgraphql.model.value.CreatedAt;
import vn.urban.vietnam.examplespringbootgraphql.model.value.TaskId;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * m_task entity class
 */
@Entity
@Table(name = "m_task")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MTask implements Serializable {
	/*********************************************
	 * Filed
	 *********************************************/
	/** タスク ID(PK) */
	@Getter
	@Id // EmbeddedIdではない EmbeddedIdなら複合キー
	@Embedded // CorporateCd 側の宣言内容などをそのまま利用
	@EqualsAndHashCode.Include
	private TaskId taskId;
	/** タスク内容 */
	@Column(nullable = false)
	private String task;
	/** 登録日時 */
	@Getter
	@Embedded // CorporateCd 側の宣言内容などをそのまま利用
	@Builder.Default
	private CreatedAt createdAt = CreatedAt.create();
}
