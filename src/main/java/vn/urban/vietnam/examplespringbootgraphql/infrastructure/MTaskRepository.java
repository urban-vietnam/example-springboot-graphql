package vn.urban.vietnam.examplespringbootgraphql.infrastructure;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import vn.urban.vietnam.examplespringbootgraphql.infrastructure.entity.MTask;
import vn.urban.vietnam.examplespringbootgraphql.model.value.TaskId;

import java.util.Optional;

/**
 * m_task access repository class
 */
public interface MTaskRepository extends JpaRepository<MTask, TaskId> {
	/**
	 * @return (task_id next val) タスクIDのふり番
	 */
	@Query("SELECT max(t.taskId) + 1 from MTask t")
	public Optional<Integer> nextTaskId();
}
