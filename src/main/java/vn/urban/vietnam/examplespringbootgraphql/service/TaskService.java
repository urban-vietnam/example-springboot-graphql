package vn.urban.vietnam.examplespringbootgraphql.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import vn.urban.vietnam.examplespringbootgraphql.infrastructure.MTaskRepository;
import vn.urban.vietnam.examplespringbootgraphql.infrastructure.entity.MTask;
import vn.urban.vietnam.examplespringbootgraphql.model.TaskFactory;
import vn.urban.vietnam.examplespringbootgraphql.model.value.CreatedAt;
import vn.urban.vietnam.examplespringbootgraphql.model.value.TaskId;
import vn.urban.vietnam.examplespringbootgraphql.types.AddTaskInput;
import vn.urban.vietnam.examplespringbootgraphql.types.Task;
import vn.urban.vietnam.examplespringbootgraphql.util.StreamUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * task service
 */
@Service
@RequiredArgsConstructor
public class TaskService {
	/*********************************************
	 * filed
	 *********************************************/
	/** repository */
	private final MTaskRepository repository;
	/** factory */
	private final TaskFactory taskFactory;

	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	/**
	 * @return Taskの 全件取得
	 */
	public List<Task> getAll() {
		// m_taskから 全件取得し 各 entity -> type へ変換(factory)し 一覧
		return StreamUtils.toStream(repository.findAll()) //  m_taskから 全件取得し
				.map(taskFactory::create)// 各 entity -> type へ変換(factory)し
				.collect(Collectors.toList());// 一覧
	}
	/**
	 * タスクの追加
	 * @param input 追加するタスクの内容
	 * @return 追加したTask
	 */
	@Transactional(rollbackFor = Exception.class) // exception -> rollback
	public Task create(AddTaskInput input) {
		// 前提条件
		if (input.getTask() == null || input.getTask().isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "追加するタスクの内容は必須です");

		// 新規登録値を生成し 新規登録実行 結果をentity -> type へ変換(factory)
		MTask task = repository.saveAndFlush(buildCreate(input)); // 新規登録値を生成し 新規登録実行
		return taskFactory.create(task); // 結果をentity -> type へ変換し返却
	}

	/**
	 * タスクの削除
	 * @param taskId 削除する対象のタスク
	 * @return 削除したTask
	 */
	@Transactional(rollbackFor = Exception.class)
	public Task remove(TaskId taskId) {
		// 前提条件
		if (taskId == null || taskId.isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "削除するタスクIDの指定は必須です");

		// 削除する内容検索
		Optional<MTask> deletedTask = repository.findById(taskId);
		// 削除する内容が存在しない場合
		if (!deletedTask.isPresent()) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "削除するタスクIDは存在しません");

		// タスクの削除実行
		repository.deleteById(taskId);
		// 削除したタスク返却
		return deletedTask
				.map(taskFactory::create)
				.get();
	}

	/*********************************************
	 * 内部参照可能関数
	 *********************************************/
	/**
	 * 新規登録値 -> DB値
	 * @param input 新規登録 値
	 * @return DB値
	 */
	public MTask buildCreate(AddTaskInput input) {
		// 新規IDの発行
		TaskId nextId = nextVal();

		// 新規登録 DB値 生成
		return MTask
				.builder()
				.taskId(nextId)
				.task(input.getTask())
				.createdAt(CreatedAt.create())
				.build();
	}
	/**
	 * @return タスクID 発行
	 */
	private TaskId nextVal() {
		// 新規登録値 のタスクID 発行
		return repository.nextTaskId().map(TaskId::of).orElse(TaskId.createFirst());
	}
}
