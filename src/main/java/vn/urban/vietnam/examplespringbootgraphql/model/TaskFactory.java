package vn.urban.vietnam.examplespringbootgraphql.model;

import org.springframework.stereotype.Component;
import vn.urban.vietnam.examplespringbootgraphql.infrastructure.entity.MTask;
import vn.urban.vietnam.examplespringbootgraphql.types.Task;

@Component
public class TaskFactory {
	/**
	 * Entity -> Type
	 * @param task タスク情報
	 * @return タスク 型 type
	 */
	public Task create(MTask task) {
		// Entity -> Type
		return Task.builder()
				.taskId(task.getTaskId())
				.task(task.getTask())
				.createdAt(task.getCreatedAt())
				.build();
	}
}
