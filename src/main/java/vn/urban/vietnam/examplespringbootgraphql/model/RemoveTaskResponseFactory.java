package vn.urban.vietnam.examplespringbootgraphql.model;

import org.springframework.stereotype.Component;
import vn.urban.vietnam.examplespringbootgraphql.types.RemoveTaskResponse;
import vn.urban.vietnam.examplespringbootgraphql.types.Task;

@Component
public class RemoveTaskResponseFactory {
	/**
	 * Entity -> Type
	 * @param removedTask 削除した タスク情報
	 * @return タスク 型 type
	 */
	public RemoveTaskResponse create(Task removedTask) {
		// Entity -> Type
		return RemoveTaskResponse.builder()
				.taskId(removedTask.getTaskId())
				.build();
	}
}
