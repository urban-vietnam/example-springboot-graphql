package vn.urban.vietnam.examplespringbootgraphql.model.value;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * domain task_id の項目 (value of m_task.task_id domain knowledge)
 */
@Embeddable
@Getter
@NoArgsConstructor(staticName = "private")
@AllArgsConstructor(staticName = "of")
public class TaskId implements Serializable {
	/*********************************************
	 * Filed
	 *********************************************/
	/** 値 (not change value) */
	@Column(name = "task_id", updatable = false)
	private Integer value;
	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	/**
	 * 生成関数
	 * @param value 文字列表現
	 * @return 生成結果
	 */
	public static TaskId of(String value) { return new TaskId(Integer.valueOf(value)); }

	/**
	 * @return 初めて登録する 新規登録 タスクID
	 */
	public static TaskId createFirst() { return new TaskId(Integer.valueOf(1)); }

	/**
	 * @return true:値 空 / false:それ以外
	 */
	public boolean isEmpty() { return value == null; }
	/**
	 * @return 文字列表現
	 */
	@Override
	public String toString() { return isEmpty() ? null : "" + value; }
}
