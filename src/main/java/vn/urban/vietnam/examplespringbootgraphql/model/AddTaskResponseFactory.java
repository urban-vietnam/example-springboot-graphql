package vn.urban.vietnam.examplespringbootgraphql.model;

import org.springframework.stereotype.Component;
import vn.urban.vietnam.examplespringbootgraphql.types.AddTaskResponse;
import vn.urban.vietnam.examplespringbootgraphql.types.Task;

@Component
public class AddTaskResponseFactory {
	/**
	 * Entity -> Type
	 * @param createdTask 新規追加した タスク情報
	 * @return タスク 型 type
	 */
	public AddTaskResponse create(Task createdTask) {
		// Entity -> Type
		return AddTaskResponse.builder()
				.taskId(createdTask.getTaskId())
				.task(createdTask.getTask())
				.createdAt(createdTask.getCreatedAt())
				.build();
	}
}
