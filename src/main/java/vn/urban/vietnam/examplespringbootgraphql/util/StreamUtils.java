package vn.urban.vietnam.examplespringbootgraphql.util;

import java.util.Collection;
import java.util.stream.Stream;

/**
 * util for Java Stream
 */
public class StreamUtils {
	/**
	 * 安全な Collection -> Stream
	 * @param collection コレクション
	 * @return 安全な Collection -> Stream
	 */
	public static <T> Stream<T> toStream(Collection<T> collection) {
		// nullまたは空の時 空Streamを返却する安全策 を施す
		return collection == null || collection.isEmpty() ? Stream.empty() : collection.stream();
	}
}
