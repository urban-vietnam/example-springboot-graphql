package vn.urban.vietnam.examplespringbootgraphql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExampleSpringbootGraphqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExampleSpringbootGraphqlApplication.class, args);
	}

}
