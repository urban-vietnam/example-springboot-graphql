package vn.urban.vietnam.examplespringbootgraphql.types;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Request input AddTaskInput (schema.graphql)
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AddTaskInput implements Serializable {
	/*********************************************
	 * Filed
	 *********************************************/
	/** 登録するタスク内容 */
	private String task;
}
