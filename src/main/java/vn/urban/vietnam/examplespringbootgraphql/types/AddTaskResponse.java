package vn.urban.vietnam.examplespringbootgraphql.types;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import vn.urban.vietnam.examplespringbootgraphql.model.value.CreatedAt;
import vn.urban.vietnam.examplespringbootgraphql.model.value.TaskId;

import java.io.Serializable;

/**
 * Response type AddTaskResponse (schema.graphql)
 */
@Getter
@Builder
@RequiredArgsConstructor
public class AddTaskResponse implements Serializable {
	/*********************************************
	 * Filed
	 *********************************************/
	/** タスク ID */
	@EqualsAndHashCode.Include
	private final TaskId taskId;
	/** タスク */
	private final String task;
	/** 登録日時 */
	private final CreatedAt createdAt;
}
