package vn.urban.vietnam.examplespringbootgraphql.types;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import vn.urban.vietnam.examplespringbootgraphql.model.value.TaskId;

import java.io.Serializable;

/**
 * Response type RemoveTaskResponse (schema.graphql)
 */
@Getter
@Builder
@RequiredArgsConstructor
public class RemoveTaskResponse implements Serializable {
	/*********************************************
	 * Filed
	 *********************************************/
	/** タスク ID */
	@EqualsAndHashCode.Include
	private final TaskId taskId;
}
