package vn.urban.vietnam.examplespringbootgraphql.resolver;

import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import vn.urban.vietnam.examplespringbootgraphql.model.AddTaskResponseFactory;
import vn.urban.vietnam.examplespringbootgraphql.model.RemoveTaskResponseFactory;
import vn.urban.vietnam.examplespringbootgraphql.model.value.TaskId;
import vn.urban.vietnam.examplespringbootgraphql.service.TaskService;
import vn.urban.vietnam.examplespringbootgraphql.types.AddTaskInput;
import vn.urban.vietnam.examplespringbootgraphql.types.AddTaskResponse;
import vn.urban.vietnam.examplespringbootgraphql.types.RemoveTaskResponse;
import vn.urban.vietnam.examplespringbootgraphql.types.Task;

/**
 * my graphql mutation resolver (type Mutation の内容一覧)
 */
@Controller
@RequiredArgsConstructor
public class MutationResolver implements GraphQLMutationResolver {
	/*********************************************
	 * filed
	 **********************************************/
	/** services */
	private final TaskService taskService;
	/** factory */
	private final AddTaskResponseFactory addTaskResponseFactory; // タスク追加 レスポンスの factory
	private final RemoveTaskResponseFactory removeTaskResponseFactory; // タスク削除 レスポンスの factory

	/*********************************************
	 * type Mutation の内容
	 **********************************************/
	/**
	 * タスク追加
	 * @param input 追加するタスクの内容
	 * @return タスク追加 のレスポンス
	 */
	public AddTaskResponse addTask(AddTaskInput input) {
		// タスクの新規追加
		Task createdTask = taskService.create(input);
		// 結果変換
		return addTaskResponseFactory.create(createdTask);
	}

	/**
	 * タスクの削除
	 * @param taskId 削除する タスクのID
	 * @return タスクの削除 のレスポンス
	 */
	public RemoveTaskResponse removeTask(TaskId taskId) {
		// タスクの削除
		Task removedTask = taskService.remove(taskId);
		// 結果変換
		return removeTaskResponseFactory.create(removedTask);
	}
}
