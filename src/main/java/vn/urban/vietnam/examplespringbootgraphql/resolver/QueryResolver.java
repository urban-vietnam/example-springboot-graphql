package vn.urban.vietnam.examplespringbootgraphql.resolver;

import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import vn.urban.vietnam.examplespringbootgraphql.service.TaskService;
import vn.urban.vietnam.examplespringbootgraphql.types.Task;

import java.util.List;

/**
 * my graphql query resolver (type Query の内容一覧)
 */
@Controller
@RequiredArgsConstructor
public class QueryResolver implements GraphQLQueryResolver {
	/*********************************************
	 * filed
	 **********************************************/
	/** services */
	private final TaskService taskService;
	/*********************************************
	 * type Query の内容
	 **********************************************/
	/**
	 * Hello world 宿題
	 * @return こんにちは世界
	 */
	public String helloWorld() { return "こんにちは世界！"; }

	/**
	 * access task table 宿題
	 * @return Task 一覧取得
	 */
	public List<Task> getTasks() { return taskService.getAll(); }
}
